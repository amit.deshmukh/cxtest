<?php

namespace Src\_Public\Utilities;

class User
{
	public static function getUserInfo()
	{
		$user_info = array();
		$access_token = Token::getSessionAccessToken(); // access token should be set in session by now
		$user_info_url = ConfigConstants::getEngagorAPIBaseUrl().'me/?access_token='.$access_token;
		$response = Utility::_file_get_contents_url($user_info_url);
		$data = Utility::_json_decode($response, true);
		if (is_array($data) && isset($data['response'])) {
			$user_info = $data['response'];
		}

		return $user_info;
	}

}