<?php

namespace Src\_Public\Utilities;

class Token
{

	public static function getSessionAccessToken()
	{
		if(isset($_SESSION) && isset($_SESSION['access_token'])) {
			return $_SESSION['access_token'];
		}
		return '';
	}

	public static function setSessionAccessToken($access_token): void
	{
		if (session_status() == PHP_SESSION_NONE) {
    	session_start();
		}

		$_SESSION['access_token'] = $access_token;
	
	}

	public static function getAccessTokenFromEngagorAPI($params = array()): string
	{
		$access_token = self::getSessionAccessToken();

		if(!$access_token && !empty($params)) {
			$token_url = ConfigConstants::getEngagorOauthBasePath().'/access_token';
			$token_url_query = Utility::_http_build_query($params);
			$token_url .= ($token_url_query) ? '?'.$token_url_query : '';
			$response = Utility::_file_get_contents_url($token_url);
			$data = Utility::_json_decode($response, true);
			
			if ($data && is_array($data) && ($data['access_token'])) {
				$access_token = $data['access_token'];
			}
		}
		
		return $access_token;
	}

}