<?php

namespace Src\_Public\Utilities;

class Account
{
	public static function getAccounts()
	{
		$accounts = array();
		$access_token = Token::getSessionAccessToken(); // access token should be set in session by now
		$url = ConfigConstants::getEngagorAPIBaseUrl().'me/accounts?access_token='.$access_token;
		$response = Utility::_file_get_contents_url($url);
		$data = Utility::_json_decode($response, true);
		if (is_array($data) && isset($data['response']) && isset($data['response']['data'])) {
			$accounts = $data['response']['data'];
		}

		return $accounts;
	}

	public static function getAccountCrisisPlans($account_id)
	{
		$crisis_plans = array();
		$access_token = Token::getSessionAccessToken(); // access token should be set in session by now
		$url = ConfigConstants::getEngagorAPIBaseUrl().$account_id.'/crisis/plans/?access_token='.$access_token;
		$response = Utility::_file_get_contents_url($url);
		$data = Utility::_json_decode($response, true);
		if(isset($data) && is_array($data) && isset($data['response'])) {
			$crisis_plans = $data['response'];
		}

		return $crisis_plans;
	}

}