<?php 

namespace Src\_Public\Utilities;

class Utility
{

	public static function _http_build_query($query = array())
	{
		return http_build_query($query);
	}

	public static function _file_get_contents($filename)
	{
		return @file_get_contents($filename);
	}

	public static function _file_get_contents_url($url)
	{
		//var_dump($url);
		$url_headers = @get_headers($url);
		//var_dump($url_headers);
		$http_response_array = array(
			'HTTP/1.1 200 OK',
			'HTTP/1.0 200 OK'
		);

		//if(count(array_intersect($http_response_array, $url_headers)) > 0){
     // at least one of $target is in $haystack
 		//}
		
		if($url_headers && is_array($url_headers) && (count(array_intersect($http_response_array, $url_headers)) > 0)) {
				return self::_file_get_contents($url);
		}
		return false;
	}

	public static function _file_get_contents_file($filename)
	{
		if(file_exists(filename)) {
			return self::_file_get_contents($filename);
		}
		return false;
	}

	public static function _json_decode($data, $assoc=false)
	{
		$json = json_decode($data, $assoc);
		if (json_last_error() === JSON_ERROR_NONE) {
    	return $json;
		}
		return false;
	}
 
}