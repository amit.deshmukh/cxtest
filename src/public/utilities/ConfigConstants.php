<?php 

namespace Src\_Public\Utilities;

//require '../public/config/cxsocial.config.php';
use \Src\_Public\Config\CXSocialConfig;

class ConfigConstants
{
	public static function getClientId(): string
	{
		//return (CLIENT_ID) ? CLIENT_ID : null;
		return (CXSocialConfig::CLIENT_ID) ? CXSocialConfig::CLIENT_ID : '';
	}

	public static function getClientSecret(): string 
	{
		return (CXSocialConfig::CLIENT_SECRET) ? CXSocialConfig::CLIENT_SECRET : '';
	}

	public static function getEngagorBaseUrl(): string
	{
		return (CXSocialConfig::ENGAGOR_APP_BASEURL) ? CXSocialConfig::ENGAGOR_APP_BASEURL : '';
	}

	public static function getEngagorAPIBaseUrl(): string
	{
		return (CXSocialConfig::ENGAGOR_API_BASEURL) ? CXSocialConfig::ENGAGOR_API_BASEURL : '';
	}

	public static function getEngagorOauthBasePath(): string
	{
		$base_url = self::getEngagorBaseUrl();
		$oauth_base_path = (CXSocialConfig::ENGAGOR_OAUTH_BASEPATH) ? CXSocialConfig::ENGAGOR_OAUTH_BASEPATH : '';
		return $base_url.$oauth_base_path;
	}

}