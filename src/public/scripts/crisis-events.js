$(document).ready(function(){
    $("input.activate-deactivate-crisis-plan").click(function() {
        let currentElement = $(this);
    	let eventUrl = currentElement.attr('eventurl');
    	let planId = currentElement.attr('planid');
    	//let planActivate = (currentElement.attr('planisactive') && (currentElement.attr('planisactive') === 'true' || currentElement.attr('planisactive') === '1')) ? false: true;
        let planActivate = true;
        if(currentElement.attr('planisactive') && 
            (currentElement.attr('planisactive') === 'true' || 
            currentElement.attr('planisactive') === '1')
        ) {
            planActivate = false;
        }

    	let postData = {"postData":{"id":planId, "activate":planActivate}, "url":eventUrl};

    	$.post("/post-api", postData, function(result, status){
            // need 2 JSON.parse. 
            // string is '{\"foo\":\"bar\"}'
            // first JSON.parse converts it to '{"foo":"bar"}'
            // second JSON.parse converts it to proper JSON {"foo":"bar"}
            let jsonRes = JSON.parse(JSON.parse(result));

            let msg = 'Success';
            if(!status) {
                msg = 'Something went wrong';
            } else {
                if(!jsonRes) {
                    msg = 'Error in getting the data';
                } else if(jsonRes.error) {
                    msg = (jsonRes.error_description) ? jsonRes.error_description : 'Some error occured';
                } else {
                    let newValue = (currentElement.val() == 'Activate') ? 'Deactivate' : 'Activate';
                    currentElement.attr('value', newValue);
                    let newValuePlanIsActive = (currentElement.val() == 'Activate') ? false: true;
                    currentElement.attr('planisactive', newValuePlanIsActive);
                }
            }

            alert(msg);
        });
    });
});
