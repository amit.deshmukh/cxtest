<?php

session_start();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Src\_Public\Utilities\ConfigConstants;
use \Src\_Public\Utilities\Utility;
use \Src\_Public\Utilities\Token;
use \Src\_Public\Utilities\User;
use \Src\_Public\Utilities\Account;

require '../../vendor/autoload.php';

$app = new \Slim\App;

$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer('./templates/');

$app->get('/', function (Request $request, Response $response, array $args) {

		// Setting config values
		$client_id = ConfigConstants::getClientId();
		$client_secret = ConfigConstants::getClientSecret();
		$scope = 'accounts_read accounts_write';

		// Setting some default values
		$view_data = array();
		$user_info = array();
		$accounts = array();
		$is_error = false;
		$error_msg = 'Something went wrong';
		
		$query_params = $request->getqueryParams();
		$code = isset($query_params['code']) ? $query_params['code'] : null;
		$error = isset($query_params['error']) ? $query_params['error'] : null;
		$state = isset($query_params['state']) ? $query_params['state'] : null;
		
		if (isset($error) && $error && $error === 'access_denied') {
			$is_error = true;
			$error_msg = 'The user did not authorize your application to use your CX Social account.';
			//exit();
		}

		if(empty($code) && !$is_error) {
			$_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
			$authorize_url_params = array(
				'client_id' => $client_id,
				'state' => $_SESSION['state'],
				'response_type' => 'code'
			);
			if($scope) {
				$authorize_url_params['scope'] = $scope;
			}

			$authorize_url = ConfigConstants::getEngagorOauthBasePath().'/authorize/';
			if($authorize_url) {
				$authorize_url_query = Utility::_http_build_query($authorize_url_params);
				$authorize_url .= ($authorize_url_query) ? '?'.$authorize_url_query : '';
				echo("<script> top.location.href='" . $authorize_url . "'; </script>");
				exit();
			} else {
				$is_error = true;
				$error_msg = 'Error in getting Engagor API Base URL.';
			}
		}
		
		if(!$is_error) {
			$access_token = Token::getSessionAccessToken();
			if(empty($access_token)) {
				$access_token = Token::getAccessTokenFromEngagorAPI(array('client_id' => $client_id, 'client_secret' => $client_secret, 'grant_type' => 'authorization_code', 'code' => $code)
				);

				if($access_token) {
					Token::setSessionAccessToken($access_token);
				} else {
					$is_error = true;
					$error_msg = 'We could not validate your access token.';
				}
			}
		}

		if (!$is_error && isset($state) && ($state !== $_SESSION['state'])) {
			$is_error = true;
			$error_msg = 'The state does not match. You may be a victim of CSRF.';
		}

		if(!$is_error) {
			$user_info = User::getUserInfo();
			$accounts = Account::getAccounts();

			// get crisis plans per account, and add it to accounts array
			for($i = 0; $i < count($accounts); $i++) {
				$account_crisis_plans = Account::getAccountCrisisPlans($accounts[$i]['id']);
				$accounts[$i]['crisis_plans'] = $account_crisis_plans;
				$accounts[$i]['crisis_event_url'] = ConfigConstants::getEngagorAPIBaseUrl().$accounts[$i]['id'].'/crisis/event/?access_token='.$access_token;
			}
		}

		$view_data['is_error'] = $is_error;
		$view_data['error_msg'] = $error_msg;
		$view_data['user_info'] = $user_info;
		$view_data['accounts'] = $accounts;
		
		return $this->view->render($response, 'index.phtml', $view_data);
});

$app->get('/logout', function(Request $request, Response $response, array $args) {
	session_destroy();
	$response->getBody()->write("You are now logged out. <br/> Want to login again? <a href=\"/\">Click Here</a>");
	return $response;
});

$app->post('/post-api', function(Request $request, Response $response, array $args) {
	$request_body = $request->getParsedBody();
	$post_data = ($request_body && $request_body['postData']) ? Utility::_http_build_query($request_body['postData']) : array();
	$url = $request_body['url'];
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	// $fp = fopen(dirname(__FILE__).'/curl_errorlog.txt', 'a'); //writing output to file
	//curl_setopt($ch, CURLOPT_VERBOSE, true);
	//curl_setopt($ch, CURLOPT_STDERR, $fp);

	$result = curl_exec($ch);
	if(curl_errno($ch)) {
		$result = false;
		// need more work to log the error, or send it back as a response so developers can debug quickly
	}

	try {
		$result = curl_exec($ch);
		if(curl_errno($ch)) {
			$result = false;
			// need more work to log the error, or send it back as a response so developers can debug quickly
		}
	} catch (Exception $e) {
		$result = false;
		// need more work to log the error, or send it back as a response so developers can debug quickly
	} finally {
		curl_close($ch);
		echo json_encode($result);
	}

});

$app->run();