<?php

use PHPUnit\Framework\TestCase;
use \Src\_Public\Utilities\Utility;

final class UtilitiesConstantTest extends TestCase
{

	public function testGetHTTPBuildQuery(): void
	{
		$this->assertEquals(Utility::_http_build_query(array('foo' => 'bar')), 'foo=bar');
		$this->assertEquals(Utility::_http_build_query(array('foo' => 'bar', 'bar' => 'foo')), 'foo=bar&bar=foo');
		$this->assertEquals(Utility::_http_build_query(array('foo' => '')), 'foo=');
		$this->assertEquals(Utility::_http_build_query(array('foo' => null)), '');
		$this->assertEquals(Utility::_http_build_query(array('foo' => null, 'bar' => 123)), 'bar=123');
		$this->assertEquals(Utility::_http_build_query(array('foo' => array())), '');
		$this->assertEquals(Utility::_http_build_query(array('foo' => array('a','b'))), 'foo%5B0%5D=a&foo%5B1%5D=b');
	}

}