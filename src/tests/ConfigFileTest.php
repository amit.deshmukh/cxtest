<?php

use PHPUnit\Framework\TestCase;

final class ConfigFileTest extends TestCase 
{
	public function testConfigFileExists(): void
  {
  	$this->assertFileExists('src/public/config/CXSocialConfig.php');
  }

  public function testConfigFileAddedToGitIgnore(): void
  {
  	// test if src/config/cxsocial.config.php has been added to .gitignore
  	$gitIgnoreFile = '.gitignore';
  	if ( file_exists($gitIgnoreFile) && ($fp = fopen($gitIgnoreFile, "rb"))!==false ) {
  		$gitIgnoreStr = fread($fp,filesize($gitIgnoreFile));
  		fclose($fp);
  		$this->assertRegexp('/src\/public\/config\/CXSocialConfig.php/', $gitIgnoreStr, '.gitignore file does not contain src/config/cxsocial.config.php. Danger of secret variables getting leaked');
  	} else {
  		$this->assertEquals(1, 2, '.gitignore file does not exist');
  	}

  }

}