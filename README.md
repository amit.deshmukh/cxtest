## Clarabridge Test

This code is used to complete an assignment given by CX Social team for the position of Full stack Developer with Clarabridge.

This project has been built using Slim Framework built in PHP,  which is installed using Composer. PHP version is 7.2.4 and Composer version is 1.7.2

### Assumptions
- I am assuming that composer is already installed on your system. You can find instructions to install composer at https://getcomposer.org/
- You can make cURL callus using PHP's cURL library

To run this project, you need to perform the following tasks. 

- Clone this repo
- cd to the repo and run composer install
- Go to src/public/config directory, create a new file called CXSocialConfig.php and copy contents of CXSocialConfig.php.sample in it. Make sure all the code is uncommented and you remove any additional helpful comments that are in the file. The file should be a valid php class.
- Create an account on CX Social or use an existing account. Copy the values for CLIENT_ID and CLIENT_SECRET in respective constants defined in CXSocialConfig.php. Update the values of other constants as necessary.
- Create virtual hosts (out of scope for this task). eg: lets assume you created a virtual host called cxsocial-test.local
- Go to cxsocial-test.local
- You will either have to log in using Oauth or you will be directly taken to home page.


### What to expect

On the home page, you will see the information about the user you are logged in as. You will see the Accounts for the user and all the crisis plans for each account. You can create events by Activating and Deactivating the crisis plans. Once you click on the button a HTTP POST request will be sent out on a separate browser tab. Then after you refresh the page you will notice that the crisis plan has been Activated or Deactivated based on the action.
